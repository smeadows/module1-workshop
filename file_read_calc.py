grand_total = 0

import operator

ops = {
    '+' : operator.add,
    '-' : operator.sub,
    'x' : operator.mul,
    '/' : operator.truediv
}

with open("Module 1 Exercise\calc_file.txt", 'r') as f:
    calc_file_list = f.read().splitlines()

for i in calc_file_list:
    calc_list_split = i.split()
    list_operator = calc_list_split[1]
    list_parm_1 = calc_list_split[2]
    list_parm_2 = calc_list_split[3]
    
    grand_total += (ops[list_operator](int(list_parm_1), int(list_parm_2)))

print(grand_total)