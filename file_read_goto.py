previous_statements = []
curr_line_number = 1

import operator

ops = {
    '+' : operator.add,
    '-' : operator.sub,
    'x' : operator.mul,
    '/' : operator.truediv
}

with open("Module 1 Exercise\goto_calc_file.txt", 'r') as f:
    goto_calc_file_list = f.read().splitlines()


while True:
    print('Processing: Current line: ' + str(curr_line_number) + ' Current statement: ' + goto_calc_file_list[curr_line_number - 1])
    goto_list_split = goto_calc_file_list[curr_line_number - 1].split()

    if goto_list_split[1] == 'calc':
        curr_line_number = int((ops[goto_list_split[2]](int(goto_list_split[3]), int(goto_list_split[4])))) #round up and down...
    else:
        curr_line_number = int(goto_list_split[1])

    if goto_calc_file_list[curr_line_number - 1] in previous_statements:
        break
    else:
        previous_statements.append(goto_calc_file_list[curr_line_number - 1])

print('Exited at Current line: ' + str(curr_line_number) + ' Current statement: ' + goto_calc_file_list[curr_line_number - 1])