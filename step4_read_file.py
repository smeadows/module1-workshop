previous_statements = []
curr_line_number = 1

import operator

ops = {
    '+' : operator.add,
    '-' : operator.sub,
    'x' : operator.mul,
    '/' : operator.truediv
}

with open("Module 1 Exercise\step_4_file.txt", 'r') as f:
    step4_file_list = f.read().splitlines()

while True:
    print('Processing: Current line: ' + str(curr_line_number) + ' Current statement: ' + step4_file_list[curr_line_number - 1])
    goto_list_split = step4_file_list[curr_line_number - 1].split()

    if goto_list_split[1] == 'calc':
        curr_line_number = int((ops[goto_list_split[2]](int(goto_list_split[3]), int(goto_list_split[4])))) #round up and down...
    elif goto_list_split[0] == 'remove':
        step4_file_list.pop(goto_list_split[1])
    elif goto_list_split[0] == 'replace':
        step4_file_list.insert(, )

    else:
        curr_line_number = int(goto_list_split[1])


    if step4_file_list[curr_line_number - 1] in previous_statements:
        break
    elif curr_line_number >= :#does this line exist?
        break
    else:
        previous_statements.append(step4_file_list[curr_line_number - 1])

print('Exited at Current line: ' + str(curr_line_number) + ' Current statement: ' + step4_file_list[curr_line_number - 1])