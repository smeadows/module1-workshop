import operator

ops = {
    '+' : operator.add,
    '-' : operator.sub,
    'x' : operator.mul,
    '/' : operator.truediv
}

while True:
    your_operation = input('Please enter an operation i.e. x, +, - or /: ')
    if your_operation in ops:
        break

while True:
    your_param_a = input('Please enter your first parameter integer : ')
    if your_param_a.isnumeric():
        break

while True:
    your_param_b = input('Please enter your second parameter integer: ')
    if your_param_b.isnumeric():
        break

print(ops[your_operation](int(your_param_a), int(your_param_b)))